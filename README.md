# module3_individual
## Running MySQL
- mysql.txt
## Tables' Fields
- Tables/students.sql
- Tables/departments.sql
- Tables/courses.sql
- Tables/grades.sql
## Queries
- Queries/Query1.txt: Select the entire grades table.
- Queries/Query2.txt: Select all fields describing the courses offered in the College of Arts & Sciences (school code L).
- Queries/Query3.txt: The names, student IDs, and CSE330 grades of all students who are in CSE330S.
- Queries/Query4.txt: The names, e-mails, and average grades of any student with an average below 50 so that the dean can send them an email notification that they are now on academic probation.
- Queries/Query5.txt: An individual report card for Jack Johnson, consisting of only his student ID, e-mail address, and average grade.
